package lcs.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lcs.util.Consumer;
import lcs.util.Producer;
import lcs.windows.AlertBox;
import lcs.windows.FileChooserWindow;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author KOlkiewicz
 */
public class Controller {

    @FXML
    TextArea patternLCS;
    @FXML
    TextField numbersThread;
    @FXML
    Button addFileButton;
    @FXML
    TextField pathFile;
    @FXML
    Label fileInfo;
    @FXML
    TextArea resultsTextArea;
    @FXML
    TextField timeWorked;
    @FXML
    Button startProgram;
    @FXML
    MenuItem menuItem;

    @FXML
    private void start() throws IOException, InterruptedException, ExecutionException {

        resultsTextArea.clear();

        if (patternLCS.getText().trim().isEmpty()) {
            AlertBox.display("Puste pole", "Proszę o uzupełnienie pola wzorca LCS");
            return;
        }

        int threadCount;
        try {
             threadCount = Integer.parseInt(numbersThread.getText());
        } catch (NumberFormatException exception){
            AlertBox.display("Nieprawidłowe dane w polu","Proszę o podanie wartości liczbowej w polu -> Podaj ilość wątków");
            numbersThread.clear();
            return;
        }

        if (threadCount <= 0){
            AlertBox.display("Nieprawidłowe dane w polu","Proszę o podanie wartości liczbowej wiekszej niż 1 w polu -> Podaj ilość wątków");
            return;
        }

        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        BlockingQueue<String> wordList = new LinkedBlockingQueue<>();

        Producer producer = new Producer(wordList, pathFile.getText());
        executorService.submit(producer);

        Set<Callable<String>> callables = new HashSet<Callable<String>>();

        for(int i = 0; i < Integer.parseInt(numbersThread.getText()); i ++)
        {
            callables.add(new Consumer(wordList, patternLCS.getText()));
        }

        long start = System.currentTimeMillis();

        List<Future<String>> futures = executorService.invokeAll(callables);

        int i=1;

        for (Future<String> future : futures){
            resultsTextArea.appendText( "Watek " + i + " wynik: " + (future.get().equals("") ? "<Nie odnaleziono wzorca.>" : future.get()) + "\n");
            i++;
        }

        long end = System.currentTimeMillis();
        String s = Objects.toString((end - start), null);

        timeWorked.setText(s);

        executorService.shutdown();
    }

    @FXML
    private void onClickAddFile(){
        FileChooserWindow fileChooserWindow = new FileChooserWindow();
        Stage addButtonStage = new Stage();
        fileChooserWindow.start(addButtonStage,pathFile,fileInfo);
    }

    @FXML
    public void exitMenuItemAction(){
        Platform.exit();
        System.exit(0);
    }

}


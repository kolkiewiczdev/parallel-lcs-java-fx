package lcs.util;

import java.io.FileNotFoundException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @author KOlkiewicz
 */
public class Consumer implements Callable<String> {

    private BlockingQueue<String> wordList;
    private String pattern;

    public Consumer(BlockingQueue<String> wordList, String pattern) {
        this.wordList = wordList;
        this.pattern = pattern;
    }

    @Override
    public String call() throws FileNotFoundException {

        boolean end = false;
        String bestResult = "";

        while (!end) {
            try {
                String word = wordList.poll(10, TimeUnit.MILLISECONDS);

                if (word != null) {
                    String tmp = LCS(pattern, word);

                    if(bestResult.length() < tmp.length())
                    {
                        bestResult = tmp;
                    }
                } else {
                    end = true;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return bestResult;
    }

    private String LCS(String pattern, String word) {
        int M = pattern.length();
        int N = word.length();

        int[][] opt = new int[M + 1][N + 1];

        for (int i = M - 1; i >= 0; i--) {
            for (int j = N - 1; j >= 0; j--) {
                if (pattern.charAt(i) == word.charAt(j))
                    opt[i][j] = opt[i + 1][j + 1] + 1;
                else
                    opt[i][j] = Math.max(opt[i + 1][j], opt[i][j + 1]);
            }
        }

        StringBuilder strBuilder = new StringBuilder();

        int i = 0, j = 0;
        while (i < M && j < N) {
            if (pattern.charAt(i) == word.charAt(j)) {
                strBuilder.append(pattern.charAt(i));
                i++;
                j++;
            } else if (opt[i + 1][j] >= opt[i][j + 1])
                i++;
            else
                j++;
        }

        return strBuilder.toString();
    }
}

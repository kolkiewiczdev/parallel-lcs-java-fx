package lcs.util;

import lcs.windows.AlertBox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.concurrent.BlockingQueue;

/**
 * @author KOlkiewicz
 */
public class Producer implements Runnable{

    private BlockingQueue<String> wordList;
    private MappedByteBuffer buffer;
    private StringBuilder strBuilder = new StringBuilder();
    private CharBuffer charBuffer;

    public Producer(BlockingQueue<String> wordList, String sourceFile) throws IOException
    {
        try {
            this.wordList = wordList;
            File file = new File(sourceFile);
            FileChannel fileChannel = new RandomAccessFile(file, "r").getChannel();
            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
            Charset charset = Charset.forName("Windows-1250");
            charBuffer = charset.decode(buffer);
        }catch (FileNotFoundException exception){
            AlertBox.display("Nie dodano pliku","Nie dodano pliku." + "\n" + "Proszę o dodanie pliku z danymi w formacie txt");
            return;
        }
    }

    @Override
    public void run() {

        while(charBuffer.hasRemaining())
        {
            char c = charBuffer.get();

            if(!Character.isWhitespace(c))
            {
                strBuilder.append(c);
            } else if(strBuilder.length() != 0){
                wordList.add(strBuilder.toString());
                strBuilder.setLength(0);
            }
        }
    }
}

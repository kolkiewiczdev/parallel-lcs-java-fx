package lcs.windows;

import javafx.scene.control.Alert;

/**
 * Created by Dom on 24.11.2016.
 */
public class AlertBox {

    public static void display(String title, String message){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText("Ostrzeżenie");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
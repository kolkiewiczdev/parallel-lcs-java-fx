package lcs.windows;


import java.io.File;
import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public final class FileChooserWindow extends Application {


    @Override
    public void start(final Stage stage){}

    public void start(final Stage stage, TextField textField, Label fileInfo) {
        final javafx.stage.FileChooser fileChooser = new javafx.stage.FileChooser();
        configureFileChooser(fileChooser);
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            textField.setText(openFile(file));
            fileInfo.setTextFill(Color.web("#2ee012"));
            fileInfo.setText("Plik został dodany");
        }
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    private String openFile(File file) {
        return file.getAbsolutePath();
    }

    private static void configureFileChooser(
            final javafx.stage.FileChooser fileChooser) {
        fileChooser.setTitle("Dodaj plik z danymi");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        fileChooser.getExtensionFilters().addAll(
                new javafx.stage.FileChooser.ExtensionFilter("Plik tekstowy", "*.txt")
        );
    }
}